import 'ol/ol.css';
import Map from 'ol/Map';
import View from 'ol/View';

import {getWidth, getTopLeft} from 'ol/extent';
import {get as getProjection} from 'ol/proj';

import {Tile as TileLayer, Vector as VectorLayer} from 'ol/layer';

import TileWMS from 'ol/source/TileWMS';
import VectorSource from 'ol/source/Vector';
import OSM from 'ol/source/OSM';

import KML from 'ol/format/KML';

import Point from 'ol/geom/Point';
import {Circle as CircleStyle, Fill, Icon, Stroke, Style} from 'ol/style';
import {getVectorContext} from 'ol/render';
import Feature from 'ol/Feature';


//
// Variables
//
////////////////////////////////////////////////////////////////////////////////
var wmsUrl = 'http://localhost:8600/geoserver/czsk/wms';

// var namespace = 'Dach:';
var namespace = 'czsk:';
var layerPrefix = namespace+'planet_osm_';

var format = 'image/png';
var bounds = [647702.25, 5746308.5,
              1908217.5, 7363116.5];
var verOld = '1.1.1'
var originOld = bounds[0] + ',' + bounds[1];

var ver = '1.3.0'
var origin = bounds[1] + ',' + bounds[0];

var geoMarker = /** @type Feature<import("../src/ol/geom/Point").default> */(new Feature({
  type: 'geoMarker',
  geometry: new Point([])
}));


//
// Styles
//
////////////////////////////////////////////////////////////////////////////////
var MarkerStyles = {
  'geoMarker': new Style({
    image: new CircleStyle({
      radius: 7,
      fill: new Fill({color: 'black'}),
      stroke: new Stroke({
        color: 'white', width: 2
      })
    })
  })
};


//
// Layers
//
////////////////////////////////////////////////////////////////////////////////
var MarkerLayer = new VectorLayer({
  source: new VectorSource({
    features: [geoMarker]
  }),
  style: function(feature) {
    return MarkerStyles[feature.get('type')];
  }
});

var KMLSource = new VectorSource({
  url: './data/kml/output.kml',
  format: new KML({
    dataProjection: 'EPSG:4326',
    featureProjection: 'EPSG:3857',
    extractStyles: true
  })
});

var KMLLayer = new VectorLayer({
  source: KMLSource,
  opacity: 1,
  visible: true
});

var KMLSourcePartial = new VectorSource({
  url: './data/kml/partial.kml',
  format: new KML({
    dataProjection: 'EPSG:4326',
    featureProjection: 'EPSG:3857',
    extractStyles: true
  })
});

var KMLLayerPartial = new VectorLayer({
  source: KMLSourcePartial,
  opacity: 1,
  visible: true
});


var OSMLayer = new TileLayer({
  source: new OSM(),
  // opacity: 0.3,
  opacity: 1,
});

var PolygonLayer = new TileLayer({
  visible: true,
  source: new TileWMS({
    url: wmsUrl,
    params: {'FORMAT': format,
             'VERSION': ver,
             tiled: true,
          "LAYERS": layerPrefix+'polygon',
          "exceptions": 'application/vnd.ogc.se_inimage',
       tilesOrigin: origin
    }
  }),
  preload: 20,
  opacity: 1
});

var PointLayer = new TileLayer({
  visible: true,
  source: new TileWMS({
    url: wmsUrl,
    params: {'FORMAT': format,
             'VERSION': ver,
             tiled: true,
          "LAYERS": layerPrefix+'point',
          "exceptions": 'application/vnd.ogc.se_inimage',
       tilesOrigin: origin
    }
  }),
  preload: 20,
  opacity: 0.2
});

var LineLayer = new TileLayer({
  visible: true,
  source: new TileWMS({
    url: wmsUrl,
    params: {'FORMAT': format,
             'VERSION': ver,
             tiled: true,
          "LAYERS": layerPrefix+'line',
          "exceptions": 'application/vnd.ogc.se_inimage',
       tilesOrigin: origin
    }
  }),
  preload: 20,
  opacity: 0.2
});

// TODO: roads are for low level rendering. Render at lower zooms only
var RoadsLayer = new TileLayer({
  visible: true,
  source: new TileWMS({
    url: wmsUrl,
    params: {'FORMAT': format,
             'VERSION': ver,
             tiled: true,
          "LAYERS": layerPrefix+'roads',
          "exceptions": 'application/vnd.ogc.se_inimage',
       tilesOrigin: origin
    }
  }),
  preload: 20,
  opacity: 0.2
});


//
// Map
//
////////////////////////////////////////////////////////////////////////////////
var defaultView =new View({
  center: [1450000, 6658099],
  projection: 'EPSG:3857',
  zoom: 5
})

const map = new Map({
  layers: [
    OSMLayer,
    // PolygonLayer, // colors
    // LineLayer, // smaller roads
    // RoadsLayer, // main roads
    KMLLayer, // directions
    KMLLayerPartial,
    MarkerLayer // marker (car) location
  ],
  view: defaultView
});


//
// Functions
//
////////////////////////////////////////////////////////////////////////////////
var displayFeatureInfo = function(pixel) {
  var features = [];
  map.forEachFeatureAtPixel(pixel, function(feature) {
    features.push(feature);
  });
  if (features.length > 0) {
    var info = [];
    var i, ii;
    for (i = 0, ii = features.length; i < ii; ++i) {
      info.push(features[i].get('name'));
    }
    document.getElementById('info').innerHTML = info.join(', ') || '(unknown)';
    map.getTarget().style.cursor = 'pointer';
  } else {
    document.getElementById('info').innerHTML = '&nbsp;';
    map.getTarget().style.cursor = '';
  }
};

map.on('pointermove', function(evt) {
  if (evt.dragging) {
    return;
  }
  var pixel = map.getEventPixel(evt.originalEvent);
  displayFeatureInfo(pixel);
});

map.on('click', function(evt) {
  displayFeatureInfo(evt.pixel);
});

// Change extent to show only route from KML file
KMLLayer.once("change", function(e){
  var extent = KMLSource.getExtent();
  var newExtent = []
  for (let index = 0; index < extent.length; index++) {
    const element = extent[index];
    if (index == 0 || index == 3) {
      newExtent.push(element + 350000);
    } else {
      newExtent.push(element - 350000);
    }
  };

  map.getView().fit(newExtent , map.getSize());
  map.setView(
    new View({
        extent: map.getView().calculateExtent(map.getSize()),
        center: defaultView.getCenter(),
        projection: defaultView.getProjection(),
        zoom: defaultView.getZoom()
      })
    );
});

function myFunction() {
  document.getElementById("frm1").submit();
}


let renderMap = async (point) => {
  try {
    // target: document.getElementById('map'),

    if (point.id == "location") {
      console.log(point);
      var lon = parseFloat(point[0].value)
      var lat = parseFloat(point[1].value)
      console.log(lon);
      console.log(lat);
      geoMarker.setGeometry(
        new Point([lon,lat]).transform('EPSG:4326', 'EPSG:3857')
      );
    }

    map.setTarget(document.getElementById('map'));
  } catch (err) {
    console.log('Error getting documents', err)
  }
  // console.log("renderMap");
  // console.log(point);
}

export default renderMap;