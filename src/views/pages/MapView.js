import renderMap from "../../services/Mapping"

let MapView = {
    render : async () => {
      let view = /*html*/`
        <section class="section">
            <div class="grid grid-pad">
                <div class="col-1-3">
                    Set car location:
                    <form id="location" action="#/map" enctype="multipart/form-data" method="post">
                        Longitude <input type="text" name="lon" multiple>
                        Latitude <input type="text" name="lat" multiple>
                        <input type="submit" value="Set">
                    </form>
                </div>
                <div class="col-1-3">
                    Find route between 2 points:
                    <form action="/" enctype="multipart/form-data" method="post">
                        Longtitude <input type="float" name="lonA" multiple>
                        Latitude <input type="float" name="latA" multiple>
                        Longtitude <input type="float" name="lonB" multiple>
                        Latitude <input type="float" name="latB" multiple>
                        <input type="submit" value="Set">
                    </form>
                </div>
                <div calss="col-1-3 push-1-3">
                    Import KML file to show directions (route):
                    <form action="/" enctype="multipart/form-data" method="post">
                        <input type="file" name="kml-file" multiple>
                        <input type="submit" value="Upload">
                    </form>
                </div>
            </div>

            <div class="mapdiv">
                <div id="map" class="map"></div>
                <div id="info">&nbsp;</div>
            </div>

        </section>
      `

      return view
    }
    , after_render: async (point) => {
        renderMap(point);
    }
}

export default MapView;